<?php
/**
 * Template Name: Map
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */
get_header();
?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">

			<div class="post-list-map-container">
				<aside class="post-list">
					<div class="post-list-title">
						<h2><?php _e('Our Trips', 'panda_siili') ?></h2>
						<button id="post-list-close"><?php echo panda_siili_get_icon_svg('chevron_left'); ?></button>
					</div>
					<ul id="post-list"></ul>

					<?php get_template_part( 'template-parts/footer/footer', 'map' ); ?>
				</aside><!-- .post-list -->

				<div class="map-container">
					<button id="post-list-toggle"><?php echo panda_siili_get_icon_svg('menu'); ?></button>
					<div id="world-map"></div>
				</div>
			</div><!-- .post-list-map-wrapper -->

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
