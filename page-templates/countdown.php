<?php
/**
 * Template Name: Countdown
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */
get_header();
?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">

			<div class="map-container">
				<div id="world-map"></div>
			</div><!-- .map-sontainer -->

			<div id="countdown">
				<div class="time-container">
					<div id="days" class="time-value">0</div>
					<div class="time-unit"><?php _e('days', 'panda_siili_2019') ?></div>
				</div>
				<div class="time-container">
					<div id="hours" class="time-value">0</div>
					<div class="time-unit"><?php _e('hours', 'panda_siili_2019') ?></div>
				</div>
				<div class="time-container">
					<div id="minutes" class="time-value">0</div>
					<div class="time-unit"><?php _e('minutes', 'panda_siili_2019') ?></div>
				</div>
				<div class="time-container">
					<div id="seconds" class="time-value">0</div>
					<div class="time-unit"><?php _e('seconds', 'panda_siili_2019') ?></div>
				</div>
			</div><!-- #countdown -->

			<?php

			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) {
					comments_template();
				}

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();