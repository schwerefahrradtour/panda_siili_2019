# Panda Siili Theme

## Translate theme

### 1. Create directory for translations

```bash
cd path/to/your/theme
mkdir languages
```

### 2. Generate POT files

Produce a template PO file with all strings to translate.

#### 2.1. Install wp-cli

```bash
wget https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp
```

#### 2.2. Generate POT file for the domain

```bash
wp i18n make-pot --domain='domain' path/to/your/theme languages/domain.pot
```

The file then consists of the following entries without translation:

```
#: file_path.php:line_no
msgid "message id" 
msgstr ""
```

### 3. Create PO file

The name of the file is `<languagesuage>_<LOCALE>.po`, e.g. `en_US.po`. Preferably copy the POT file generated in the previous step and just add the translations to `msgstr` fields.

```bash
cp languages/domain.pot languages/sk_SK.po
```

### 4. Generate MO files

```bash
for f in $(find languages -name '*.po'); do msgfmt -o ${f%.*}.mo $f; done
```

### 5. Load domain translations

To `[theme/]functions.php` add the following line:

```php
load_theme_textdomain('domain', get_template_directory() . '/languages'));
```

### 6. Update PO file from POT

#### 6.1. Install Poedit

```bash
sudo apt install poedit
```

#### 6.2. Update in Poedit

1. Run Poedit
2. Edit a translation -> Choose a PO file ...
3. Catalog -> Update from POT file ...
4. Add the missing translations
5. Save
6. File -> Compile to MO ...
