# How to Install ...

## 1. LAMP

### 1.1 Apache

```bash
sudo apt install apache2
```

### 1.2 MySQL

The installation is too secure for localhost testing server. Some questions can be answered 'no'.

```bash
sudo apt install mysql-server
sudo mysql_secure_installation
sudo mysql
```

When setting the password, use something else than 'password'.

```mysql
SELECT user,authentication_string,plugin,host FROM mysql.user;
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';
FLUSH PRIVILEGES;
```

### 1.3 PHP

```bash
sudo apt install php libapache2-mod-php php-mysql
sudo apt install php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip
sudo systemctl status apache2
sudo vim /var/www/html/index.php
```

`/var/www/html/index.php`:

```php
<?php
phpinfo();
?>
```

`http://your_local_ip_address/info.php`

```bash
sudo rm /var/www/html/info.php
```

## 2. LAMP for Wordpress

### 2.1 Create MySQL Database and User

```bash
mysql -u root -p
```

Replace 'password' with your real password.

```mysql
CREATE DATABASE schwerefahrradtour DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
SET GLOBAL validate_password_policy=LOW;
CREATE USER 'fic'@'localhost' IDENTIFIED BY 'password';
GRANT ALL ON schwerefahrradtour.* TO 'fic'@'localhost';
FLUSH PRIVILEGES;
```

### 2.2 Install PHP Extensions

```bash
sudo apt install php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip
sudo systemctl restart apache2
```

### 2.3 Adjust Apache Configuration

Enable rewrite module.

```bash
sudo vim /etc/apache2/sites-available/schwerefahrradtour.conf
```

```
<Directory /var/www/html/schwerefahrradtour/>
    AllowOverride All
</Directory>
```

or

```
<VirtualHost *:80>
    ServerName schwerefahrradtour.local
    DocumentRoot /var/www/html/schwerefahrradtour
    <Directory /var/www/html/schwerefahrradtour>
        Options FollowSymLinks
        AllowOverride Limit Options FileInfo
        DirectoryIndex index.php
        Require all granted
    </Directory>
    <Directory /var/www/html/schwerefahrradtour/wp-content>
        Options FollowSymLinks
        Require all granted
    </Directory>
</VirtualHost>

```

```bash
sudo a2enmod rewrite
sudo apache2ctl configtest
sudo systemctl restart apache2
```

### 2.4 add the server name to `/etc/hosts`

```
127.0.0.1    schwerefahrradtour.local
```

## 3. Wordpress

### 3.1 Download and unzip

From https://wordpress.org/download/.

```bash
unzip ~/Downloads/wordpress-4.9.8.zip
mv wordpress/* .
rmdir wordpress
```

### 3.2 Link the repository to `/var/www/html`

```bash
sudo ln -s /home/fic/gitlab/schwerefahrradtour /var/www/html
```

### 3.3 Set the Owner and Access Rights

After this you must log off and on.

```bash
sudo chown -RL www-data:www-data /var/www/html/schwerefahrradtour
sudo chmod -R 664 /var/www/html/schwerefahrradtour
sudo chmod -R a+X /var/www/html/schwerefahrradtour
sudo adduser fic www-data
```

### 3.4 Follow Wordpress Web Interface

`localhost/schwerefahrradtour`

## 5. Migrate to a New Domain

### 5.1. Backup Files and Database

TODO

### 5.2. Update URL in Database

```mysql
SET @oldurl = 'http://schwerefahrradtour.org' COLLATE utf8mb4_unicode_520_ci;
SET @newurl = 'https://schwerefahrradtour.org' COLLATE utf8mb4_unicode_520_ci;
UPDATE wp_options SET option_value = replace(option_value, @oldurl, @newurl) WHERE option_name = 'home' OR option_name = 'siteurl';
UPDATE wp_posts SET guid = replace(guid, @oldurl, @newurl);
UPDATE wp_posts SET post_content = replace(post_content, @oldurl, @newurl);
UPDATE wp_postmeta SET meta_value = replace(meta_value, @oldurl, @newurl);
```

or run a sed command on the exported `*.sql` file

```bash
sed 's|https://schwerefahrradtour.org|http://schwerefahrradtour.local|g' "2023-08-02_schwerefahrradtour.sql" > "2023-08-05_sftdev.sql"
```

## 6. Set up SCSS Support in Sublime Text

### 6.1. Install Package Control

```
Ctrl+Shift+P -> Install: Package Control
```

### 6.2. Install sass

```bash
sudo apt install sass
```

### 6.3. Install Sass and SASS Build

```
Ctrl+Shift+P -> Package Control: Install Package -> Sass, SASS Build
```

### 6.4. Build the css

```
style.scss:
Tools -> Build With... (Ctrl+Shift+B) -> Sass [- Compressed]
Tools -> Build (Ctrl+B) -> style.css, style.css.map
```

## 7. Set up SCSS Support in Visual Studio Code

### 7.1. Install sass

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
nvm install --lts
npm install -g node-sass
```

### 7.2 Create tasks.json

Create `tasks.json` in `.vscode` directory and copy the following content

```json
// Sass configuration
{
  // See https://go.microsoft.com/fwlink/?LinkId=733558
  // for the documentation about the tasks.json format
  "version": "2.0.0",
  "tasks": [
    {
      "label": "Sass Compile",
      "type": "shell",
      "command": "node-sass style.scss style.css",
      "group": "build"
    }
  ]
}
```

### 7.3 Build

Hit Ctrl+Shift+B, choose to scan for errors of node-sass compiler.
