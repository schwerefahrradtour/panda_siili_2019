# Copyright (C) 2019 the WordPress team
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Twenty Nineteen 1.2\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/theme/panda_siili_2019\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2019-12-28T13:37:21+01:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.4.0\n"
"X-Domain: panda_siili_2019\n"

#. Theme Name of the theme
msgid "Twenty Nineteen"
msgstr ""

#. Theme URI of the theme
msgid "https://github.com/WordPress/twentynineteen"
msgstr ""

#. Description of the theme
msgid "Our 2019 default theme is designed to show off the power of the block editor. It features custom styles for all the default blocks, and is built so that what you see in the editor looks like what you'll see on your website. Twenty Nineteen is designed to be adaptable to a wide range of websites, whether you’re running a photo blog, launching a new business, or supporting a non-profit. Featuring ample whitespace and modern sans-serif headlines paired with classic serif body text, it's built to be beautiful on all screen sizes."
msgstr ""

#. Author of the theme
msgid "the WordPress team"
msgstr ""

#. Author URI of the theme
msgid "https://wordpress.org/"
msgstr ""

#. Template Name of the theme
msgid "Countdown"
msgstr ""

#: page-templates/countdown.php:23
msgid "days"
msgstr ""

#: page-templates/countdown.php:27
msgid "hours"
msgstr ""

#: page-templates/countdown.php:31
msgid "minutes"
msgstr ""

#: page-templates/countdown.php:35
msgid "seconds"
msgstr ""

#: theme/functions.php:29
msgid "hut"
msgstr ""

#: theme/functions.php:30
msgid "expired"
msgstr ""
