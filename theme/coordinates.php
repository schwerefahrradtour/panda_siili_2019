<?php

// custom location meta box with lat & lon fields
function location_meta_box( $post ) {

  wp_nonce_field( basename( __FILE__ ), 'location_nonce' );

  echo '<p>';
  echo '<label for="latitude">' . _e( "Latitude", 'twentyseventeen' ) . '</label>';
  echo '<br />';
  echo '<input class="widefat" type="text" name="latitude" id="latitude" value="' . esc_attr( get_post_meta( $post->ID, 'latitude', true ) ) . '" size="30" />';
  echo '</p>';
  echo '<p>';
  echo '<label for="longitude">' . _e( "Longitude", 'twentyseventeen' ) . '</label>';
  echo '<br />';
  echo '<input class="widefat" type="text" name="longitude" id="longitude" value="' . esc_attr( get_post_meta( $post->ID, 'longitude', true ) ) . '" size="30" />';
  echo '</p>';
}
function add_post_meta_boxes() {
	add_meta_box('location', 'Location', 'location_meta_box', 'post', 'side', 'default');
}
function save_location_meta($post_id, $post) {
	// verify the ?nonce?
	if (!isset($_POST['location_nonce']) || !wp_verify_nonce($_POST['location_nonce'], basename(__FILE__)))
		return $post_id;

	// get the post type object
	$post_type = get_post_type_object($post->post_type);

	// check if the current user has permission to edit the post
	if (!current_user_can($post_type->cap->edit_post, $post_id))
		return $post_id;

	$field_names = ['longitude', 'latitude'];
	foreach ($field_names as $field_name) {
		// get the new field value
		$new_field_value = isset($_POST[$field_name]) ? $_POST[$field_name] : ’;
		// get the old field value
		$field_value = get_post_meta($post_id, $field_name, true);
		// the value is new -> add
		if ($new_field_value && $field_value == '')
			add_post_meta($post_id, $field_name, $new_field_value, true);
		// the value is changed -> update
		elseif ($new_field_value && $new_field_value != $field_value)
			update_post_meta($post_id, $field_name, $new_field_value);
		// the value was erased -> delete
		elseif ($new_field_value == '' && $field_value)
			delete_post_meta($post_id, $field_name, $field_value);
	}
}
function post_meta_boxes_setup() {
	add_action('add_meta_boxes', 'add_post_meta_boxes');
	add_action('save_post', 'save_location_meta', 10, 2);
}
add_action( 'load-post.php', 'post_meta_boxes_setup' );
add_action( 'load-post-new.php', 'post_meta_boxes_setup' );

// expose the post meta in wp rest api
function create_api_post_meta() {
	// register_rest_field ( 'name-of-post-type', 'name-of-field-to-return', array-of-callbacks-and-schema() )
	register_rest_field('post', 'meta', array(
		'get_callback' => function($object) {
			return get_post_meta($object['id']);
		},
		'schema' => null
	));
}
add_action('rest_api_init', 'create_api_post_meta');
