function adjustMapHeight(mapContainer, offset = 0) {
	// if admin bar is visible, adjust the height
	let wpAdminBar = document.getElementById('wpadminbar');
	let wpAdminBarHeight = 0;
	if (wpAdminBar) {
		wpAdminBarHeight = wpAdminBar.clientHeight;
	}
	let windowHeight = window.innerHeight;
	// let headerBottom = document.getElementById('masthead').getBoundingClientRect().bottom;
	let headerHeight = document.getElementById('masthead').clientHeight + 6; // borderWidth: 	6px 
	let mapHeight = windowHeight - wpAdminBarHeight - headerHeight - offset;
	mapContainer.style.height = mapHeight + 'px';
}
