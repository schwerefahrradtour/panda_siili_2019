(function() {

	var map,
		postList = document.getElementById('post-list');

	function loadMap() {
		initMapMapbox();
		wp.api.loadPromise.done(function() {
			let postsCollection = new wp.api.collections.Posts();
			postsCollection.fetch({success: buildPostListAddPostLayer});
		});
	}

	function initMapMapbox() {
		adjustMapHeight();
		if (typeof mapboxgl === 'undefined') {
			return
		}
		mapboxgl.accessToken = 'pk.eyJ1IjoiZmljaW5hdG9yIiwiYSI6ImNqbnJ4NmJmdzBkMXUzcXMxd2oybG9jazEifQ.SWy0XOT7s9aNd_WHUhF48w';
		map = new mapboxgl.Map({
			container: 'world-map',
			style: 'mapbox://styles/ficinator/cjo4tbifh028b2sqrkbyb2aso',
			center: [16.3707, 48.2082],
			zoom: 3
		});
	}

	function buildPostListAddPostLayer(postsCollection) {
		let postsGeojson = getPostsGeoJson(postsCollection);
		// add post items into the list
		buildPostList(postsGeojson);
		// display layer with post markers on the map
		addPostLayer(postsGeojson);
	}

	function buildPostList(postsGeojson) {
		// Iterate through the list of posts
		for (let i in postsGeojson.features) {
			let postFeature = postsGeojson.features[i];
			// Select the post-list container in the HTML and append a li
			// with the class 'post-item' for each store
			let postItem = postList.appendChild(document.createElement('li'));
			postItem.className = 'post-item';
			postItem.id = 'post-item-' + postFeature.properties.slug;

			// Create a new heading with the class 'title' for each post
			let title = postItem.appendChild(document.createElement('h3'));
			title.className = 'post-title';
			title.dataPosition = i;
			title.innerHTML = postFeature.properties.title;

			// create link to the post itself
			let link = postItem.appendChild(document.createElement('a'));
			link.href = postFeature.properties.link
			link.className = 'post-read-more'
			// TODO: solve localization
			link.innerHTML = 'Read More'

			// add listener to fly to map location and highlight to item in the post list
			title.addEventListener('click', function(e) {
				let clickedPost = postsGeojson.features[this.dataPosition];
				flyToPost(clickedPost);
				highlightPostItem(this.parentNode);
				showPopUp(clickedPost);
			});
		}
	}

	function addPostLayer(postsGeojson) {
		if (typeof map === 'undefined') {
			return
		}
		let layerId = 'posts';
		// TODO: doesn't work with on_load :/
		map.on('load', function() {
			map.loadImage(uri.theme_file + '/images/banana-icon.png', function(error, image) {
				map.addImage('banana', image);
				map.addLayer({
					id: layerId,
					type: 'symbol',
					// type: 'circle',
					source: {
						type: 'geojson',
						data: postsGeojson,
					},
					layout: {
						'icon-image': 'banana',
						'icon-size': 0.5,
						'icon-allow-overlap': true,
					}
				});
			});
		});

		// add listener to fly to map location and highlight to item in the post list
		map.on('click', function(e) {
			// Query all the rendered points in the view
			let posts = map.queryRenderedFeatures(e.point, { layers: [layerId] });
			if (posts.length) {
				let clickedPost = posts[0];
				// 1. Fly to the point
				flyToPost(clickedPost);
				// 2. Highlight item in sidebar (and remove highlight for all other items)
				let clickedSlug = clickedPost.properties.slug;
				let postItem = document.getElementById('post-item-' + clickedSlug)
				highlightPostItem(postItem);
				showPopUp(clickedPost);
			}
		});
	}

	function adjustMapHeight() {
		// if admin bar is visible, adjust the height
		let wpAdminBar = document.getElementById('wpadminbar');
		let wpAdminBarHeight = 0;
		if (wpAdminBar) {
			wpAdminBarHeight = wpAdminBar.clientHeight;
		}
		let postListMapContainer = document.querySelector('.post-list-map-container');
		let windowHeight = window.innerHeight;
		// let headerBottom = document.getElementById('masthead').getBoundingClientRect().bottom;
		let headerHeight = document.getElementById('masthead').clientHeight;
		let mapHeight = windowHeight - wpAdminBarHeight - headerHeight;
		postListMapContainer.style.height = mapHeight + 'px';
	}

	function getPostsGeoJson(postsCollection) {
		let features = [];
		for (let post of postsCollection.models) {
			let attribs = post.attributes;
			let meta = attribs.meta;
			if (meta.latitude && meta.longitude) {
				features.push({
					'type': 'Feature',
					'properties': {
						'title': attribs.title.rendered,
						'link': attribs.link,
						'slug': attribs.slug
					},
					'geometry': {
						'type': 'Point',
						'coordinates': [meta.longitude[0], meta.latitude[0]]
					  }
				});
			}
		}
		return {'type': 'FeatureCollection', 'features': features}
	}

	function flyToPost(post) {
		map.flyTo({
			center: post.geometry.coordinates,
			zoom: 10,
		});
	}

	function highlightPostItem(postItem) {
		let highlightedItems = postList.querySelector('.highlighted');
		if (highlightedItems) {
			highlightedItems.classList.remove('highlighted');
		}
		postItem.classList.add('highlighted');
	}
	function showPopUp(post) {
		let popUps = document.getElementsByClassName('mapboxgl-popup');
		// Check if there is already a popup on the map and if so, remove it
		if (popUps[0]) {
			popUps[0].remove();
		}

		let title = post.properties.title;
		let link = post.properties.link;

		let popUp = new mapboxgl.Popup({ closeOnClick: false })
			.setLngLat(post.geometry.coordinates)
			.setHTML('<h3>' + title + '</h3><div class="post-read-more"><a href="' + link + '">Read More</a></div>')
			.addTo(map);
	}

	// on document load, load the map
	document.addEventListener('DOMContentLoaded', function() {
		loadMap();
	});

	// not used anymore
	function drawPosts(posts) {
		for (let post of posts.models) {
			let meta = post.attributes.meta;
			if (meta.latitude && meta.longitude) {
				let marker = new mapboxgl.Marker()
					.setLngLat([meta.longitude[0], meta.latitude[0]])
					.setPopup(new mapboxgl.Popup().setHTML(
					'<a href="' + post.attributes.link + '">' + post.attributes.title.rendered + '</a>'))
				.addTo(map);
			}
		}
	}

})();
